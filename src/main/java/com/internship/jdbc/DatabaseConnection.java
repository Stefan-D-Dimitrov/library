package com.internship.jdbc;

import com.internship.log.Log;
import com.internship.utility.PropertyReader;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

public class DatabaseConnection {
    private static DatabaseConnection instance;
    private static Map<String, String> values;
    private Log log = Log.getInstance();

    private DatabaseConnection() {
        PropertyReader propertyReader = new PropertyReader();
        try {
            values = propertyReader.getPropValues();
        } catch (IOException e) {
            log.getLogger().info("Properties file not found");
        }
    }

    public static DatabaseConnection getInstance() {
        if (instance == null) {
            instance = new DatabaseConnection();
        }
        return instance;
    }

    public Connection connect() throws ClassNotFoundException, SQLException {
        Class.forName(values.get("driver"));
        return DriverManager.getConnection(values.get("url"), values.get("username"), values.get("password"));
    }
}
