package com.internship.log;

import org.apache.log4j.PropertyConfigurator;

public class Log {
    private static Log instance;
    private static org.apache.log4j.Logger logger;

    private Log() {
        logger = org.apache.log4j.Logger.getLogger(Log.class.getName());
        PropertyConfigurator.configure("src/main/resources/log4j.properties");
    }

    public static Log getInstance() {
        if(instance == null){
            instance = new Log();
        }
        return instance;
    }

    public org.apache.log4j.Logger getLogger() {
        return logger;
    }
}
