package com.internship.servlets;

import com.internship.entity.Author;
import com.internship.log.Log;
import com.internship.service.AuthorService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name="author")
public class AuthorServlet extends HttpServlet {
    private Log log = Log.getInstance();
    private AuthorService authorService = new AuthorService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.getLogger().info("Starting GET Author Servlet");
        if(!req.getParameter("id").equals("all")){
            Author author = authorService.selectById(Integer.parseInt(req.getParameter("id")));
            if(author != null) {
                System.out.println(author.toString());
            }else {
                log.getLogger().error("No such Author");
            }
        }else {
            List<Author> itemList = authorService.getAllAuthors();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.getLogger().info("Starting POST Author servlet");
        Author author = new Author(req.getParameter("name"));
        authorService.insertAuthor(author);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.getLogger().info("Starting Delete Author Servlet");
        try {
            authorService.deleteById(Integer.parseInt(req.getParameter("id")));
        }catch (NumberFormatException e){
            log.getLogger().error("Id is not a number");
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.getLogger().info("Starting PUT Author Servlet");
        authorService.updateAuthor(Integer.parseInt(req.getParameter("id")), new String[]{req.getParameter("name")});
    }
}
