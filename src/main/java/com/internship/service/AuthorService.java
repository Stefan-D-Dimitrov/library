package com.internship.service;

import com.internship.dao.AuthorDao;
import com.internship.entity.Author;

import java.util.List;

public class AuthorService {
    private AuthorDao authorDao = AuthorDao.getInstance();


    public List<Author> getAllAuthors(){
        return authorDao.selectAll();
    }

    public Author selectById(long id){
        return authorDao.select(id);
    }

    public void insertAuthor(Author author){
        authorDao.insert(author);
    }

    public void deleteById(long id){
        authorDao.delete(id);
    }

    public void updateAuthor(long id, String[] params){
        authorDao.update(id, params);
    }
}
