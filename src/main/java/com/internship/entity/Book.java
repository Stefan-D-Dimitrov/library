package com.internship.entity;

public class Book {
    private Integer bookId;
    private String name;
    private Integer authorId;
    private Integer year;
    private Integer publisherId;
    private Integer genreId;

    public Book(String name, Integer authorId, Integer year, Integer publisherId, Integer genreId){
        this.name = name;
        this.authorId = authorId;
        this.year = year;
        this.publisherId = publisherId;
        this.genreId = genreId;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Integer publisherId) {
        this.publisherId = publisherId;
    }

    public Integer getGenreId() {
        return genreId;
    }

    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }
}
