package com.internship.utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertyReader {
    private InputStream inputStream;

    public Map<String,String> getPropValues() throws IOException {
        Map<String, String> info = new HashMap<>();
        try {
            Properties prop = new Properties();
            String propFileName = "src/main/resources/connection.properties";
            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            info.put("driver",prop.getProperty("driver"));
            info.put("url",prop.getProperty("url"));
            info.put("username",prop.getProperty("username"));
            info.put("password",prop.getProperty("password"));
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            if(inputStream != null)
                inputStream.close();
        }
        return info;
    }
}
