package com.internship.dao;

import java.util.List;

public interface Dao<T> {
    T select(long id);

    List<T> selectAll();

    void insert(T t);

    void update(long id, String[] params);

    void delete(long id);
}
