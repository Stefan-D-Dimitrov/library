package com.internship.dao;

import com.internship.entity.Publisher;
import com.internship.jdbc.DatabaseConnection;
import com.internship.log.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PublisherDao implements Dao<Publisher>{
    private Log log = Log.getInstance();
    private Connection conn = null;
    private static PublisherDao instance;
    private static DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

    public static PublisherDao getInstance() {
        if (instance == null) {
            instance = new PublisherDao();
        }
        return instance;
    }

    @Override
    public Publisher select(long id) {
        Publisher publisher = null;
        try {
            conn = databaseConnection.connect();

            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM \"Library\".\"Publisher\" WHERE \"publisher_id\" = " + id);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                publisher = new Publisher(rs.getString("name"));
                publisher.setPublisherId(rs.getInt("publisher_id"));
            }
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (ClassNotFoundException ex) {
            log.getLogger().error(ex.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sqlException) {
                log.getLogger().error(sqlException.getMessage());
            }
        }
        return publisher;
    }

    @Override
    public List<Publisher> selectAll() {
        List<Publisher> itemList = new ArrayList<>();
        String query = "SELECT * FROM \"Library\".\"Publisher\"";
        try {
            conn = databaseConnection.connect();
            PreparedStatement pstmt = conn.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Publisher publisher = new Publisher(rs.getString("name"));
                publisher.setPublisherId(rs.getInt("publisher_id"));
                itemList.add(publisher);
            }
        } catch (ClassNotFoundException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.getLogger().error(e.getMessage());
            }
        }
        return itemList;
    }

    @Override
    public void insert(Publisher publisher) {
        try {
            conn = databaseConnection.connect();
            String query = "INSERT INTO \"Library\".\"Publisher\"(name) VALUES (?)";

            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, publisher.getName());

            pstmt.executeUpdate();
        } catch (ClassNotFoundException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.getLogger().error(e.getMessage());
            }
        }
    }

    @Override
    public void update(long id, String[] params) {

    }

    @Override
    public void delete(long id) {
        try {
            conn = databaseConnection.connect();
            String query = "DELETE FROM \"Library\".\"Publisher\" la WHERE la.id = (?);";

            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setLong(1,id);

            pstmt.executeUpdate();
        } catch (ClassNotFoundException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.getLogger().error(e.getMessage());
            }
        }
    }
}
