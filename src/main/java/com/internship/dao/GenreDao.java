package com.internship.dao;

import com.internship.entity.Genre;
import com.internship.jdbc.DatabaseConnection;
import com.internship.log.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GenreDao implements Dao<Genre>{
    private Log log = Log.getInstance();
    private Connection conn = null;
    private static GenreDao instance;
    private static DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

    public static GenreDao getInstance() {
        if (instance == null) {
            instance = new GenreDao();
        }
        return instance;
    }

    @Override
    public Genre select(long id) {
        Genre genre = null;
        try {
            conn = databaseConnection.connect();

            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM \"Library\".\"Genre\" WHERE \"genre_id\" = " + id);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                genre = new Genre(rs.getString("name"));
                genre.setGenreId(rs.getInt("genre_id"));
            }
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (ClassNotFoundException ex) {
            log.getLogger().error(ex.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sqlException) {
                log.getLogger().error(sqlException.getMessage());
            }
        }
        return genre;
    }

    @Override
    public List<Genre> selectAll() {
        List<Genre> itemList = new ArrayList<>();
        String query = "SELECT * FROM \"Library\".\"Genre\"";
        try {
            conn = databaseConnection.connect();
            PreparedStatement pstmt = conn.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Genre genre = new Genre(rs.getString("name"));
                genre.setGenreId(rs.getInt("genre_id"));
                itemList.add(genre);
            }
        } catch (ClassNotFoundException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.getLogger().error(e.getMessage());
            }
        }
        return itemList;
    }

    @Override
    public void insert(Genre genre) {
        try {
            conn = databaseConnection.connect();
            String query = "INSERT INTO \"Library\".\"Genre\"(name) VALUES (?)";

            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, genre.getName());

            pstmt.executeUpdate();
        } catch (ClassNotFoundException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.getLogger().error(e.getMessage());
            }
        }
    }

    @Override
    public void update(long id, String[] params) {

    }

    @Override
    public void delete(long id) {
        try {
            conn = databaseConnection.connect();
            String query = "DELETE FROM \"Library\".\"Genre\" la WHERE la.id = (?);";

            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setLong(1,id);

            pstmt.executeUpdate();
        } catch (ClassNotFoundException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.getLogger().error(e.getMessage());
            }
        }
    }
}
