package com.internship.dao;

import com.internship.entity.Author;
import com.internship.jdbc.DatabaseConnection;
import com.internship.log.Log;

import java.sql.*;
import java.util.*;

public class AuthorDao implements Dao<Author> {
    private Log log = Log.getInstance();
    private Connection conn = null;
    private static AuthorDao instance;
    private static DatabaseConnection databaseConnection = DatabaseConnection.getInstance();

    public static AuthorDao getInstance() {
        if (instance == null) {
            instance = new AuthorDao();
        }
        return instance;
    }

    @Override
    public Author select(long id) {
        Author author = null;
        try {
            conn = databaseConnection.connect();

            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM \"Library\".\"Author\" WHERE \"author_id\" = " + id);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                author = new Author(rs.getString("name"));
                author.setAuthorId(rs.getInt("author_id"));
            }
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (ClassNotFoundException ex) {
            log.getLogger().error(ex.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException sqlException) {
                log.getLogger().error(sqlException.getMessage());
            }
        }
        return author;
    }

    @Override
    public List<Author> selectAll() {
        List<Author> itemList = new ArrayList<>();
        String query = "SELECT * FROM \"Library\".\"Author\"";
        try {
            conn = databaseConnection.connect();
            PreparedStatement pstmt = conn.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Author author = new Author(rs.getString("name"));
                author.setAuthorId(rs.getInt("author_id"));
                itemList.add(author);
            }
        } catch (ClassNotFoundException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.getLogger().error(e.getMessage());
            }
        }
        return itemList;
    }

    @Override
    public void insert(Author author) {
        try {
            conn = databaseConnection.connect();
            String query = "INSERT INTO \"Library\".\"Author\"(name) VALUES (?)";

            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, author.getName());

            pstmt.executeUpdate();
        } catch (ClassNotFoundException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.getLogger().error(e.getMessage());
            }
        }
    }

    @Override
    public void update(long id, String[] params) {
        try {
            conn = databaseConnection.connect();
            String query = "UPDATE \"Library\".\"Author\" la SET name=? WHERE la.author_id=?;";

            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1,params[0]);
            pstmt.setLong(2,id);
            pstmt.executeUpdate();

        } catch (ClassNotFoundException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.getLogger().error(e.getMessage());
            }
        }
    }

    @Override
    public void delete(long id) {
        try {
            conn = databaseConnection.connect();
            String query = "DELETE FROM \"Library\".\"Author\" la WHERE la.id = (?);";

            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setLong(1,id);

            pstmt.executeUpdate();
        } catch (ClassNotFoundException exception) {
            log.getLogger().error(exception.getMessage());
        } catch (SQLException exception) {
            log.getLogger().error(exception.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                log.getLogger().error(e.getMessage());
            }
        }
    }
}