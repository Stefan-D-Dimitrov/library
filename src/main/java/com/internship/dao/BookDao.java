package com.internship.dao;

import com.internship.entity.Book;
import com.internship.jdbc.DatabaseConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDao implements Dao<Book>{
    private static BookDao instance;
    private DatabaseConnection databaseConnection = DatabaseConnection.getInstance();
    private Connection conn = null;

    public static BookDao getInstance() {
        if (instance == null) {
            instance = new BookDao();
        }
        return instance;
    }

    @Override
    public Book select(long id) {
        Book book = null;
        try {
            conn = databaseConnection.connect();

            PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM \"Library\".\"Book\" WHERE \"book_id\" = " + id);
            ResultSet rs = pstmt.executeQuery();

            while(rs.next()){
               // book = new Book(rs.getString("name"));
               // book.setBookId(rs.getInt("Book_id"));
            }
        }catch(SQLException exception){
            //TODO log
        }catch(ClassNotFoundException ex){
            //TODO log
            System.out.println(ex.getMessage());
        } finally {
            try{
                if(conn != null){
                    conn.close();
                }
            }catch(SQLException sqlException){
                //TODO LOG
                System.out.println(sqlException.getMessage());
            }
        }
        return book;
    }

    @Override
    public List<Book> selectAll(){
        List<Book> itemList = new ArrayList<>();
        String query = "SELECT * FROM \"Library\".\"Book\"";
        try{
            conn = databaseConnection.connect();
            PreparedStatement pstmt = conn.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                //Book book = new Book(rs.getString("name"));
                //Book.setBookId(rs.getInt("Book_id"));
                //itemList.add(Book);
            }
        } catch(ClassNotFoundException exception){
            //TODO fix
            System.out.println(exception.getMessage() + " Class not found");
        } catch(SQLException exception){
            //TODO fix
            System.out.println(exception.getMessage() + " SQLException");
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch (SQLException e) {
                //TODO fix
                System.out.println(e.getMessage());
            }
        }

        return itemList;
    }

    @Override
    public void insert(Book book) {
        try{
            conn = databaseConnection.connect();
            String query = "INSERT INTO \"Library\".\"Book\"(name, author_id, year, publisher_id, genre_id) VALUES (?, ?, ?, ?, ?);";

            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1,book.getName());

            pstmt.executeUpdate();
        } catch(ClassNotFoundException exception){
            //TODO fix
            System.out.println(exception.getMessage() + " Class not found");
        } catch(SQLException exception){
            //TODO fix
            System.out.println(exception.getMessage() + " SQLException");
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch (SQLException e) {
                //TODO fix
                System.out.println(e.getMessage());
            }
        }
    }

    @Override
    public void update(long id, String[] params) {

    }

    @Override
    public void delete(long id) {
        try{
            conn = databaseConnection.connect();
            String query = "DELETE FROM \"Library\".\"Book\" lb WHERE lb.book_id = (?);";

            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setLong(1,id);

            pstmt.executeUpdate();
        } catch(ClassNotFoundException exception){
            //TODO fix
            System.out.println(exception.getMessage() + " Class not found");
        } catch(SQLException exception){
            //TODO fix
            System.out.println(exception.getMessage() + " SQLException");
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch (SQLException e) {
                //TODO fix
                System.out.println(e.getMessage());
            }
        }
    }
}
